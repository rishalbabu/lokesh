package yeppypot.mychildclasslib.core.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.greenrobot.eventbus.EventBus;

import yeppypot.mychildclasslib.core.messages.NetworkMessage;
import yeppypot.mychildclasslib.core.utils.NetworkUtil;

public class NetworkBroadCastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        int status = NetworkUtil.getConnectivityStatus(context);
        NetworkMessage message = new NetworkMessage(status > 0);
        EventBus.getDefault().post(message);
    }
}
