package yeppypot.mychildclasslib.core.adapters.recycleradapter;

public interface LongClickHandler<T>
{
    void onLongClick(T viewModel);
}
