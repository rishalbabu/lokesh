package yeppypot.mychildclasslib.core.adapters.recycleradapter.binder;

public interface ItemBinder<T>
{
      int getLayoutRes(T model);
      int getBindingVariable(T model);
}
