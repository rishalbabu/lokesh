package yeppypot.mychildclasslib.core.utils;

/**
 * Created by Lokesh on 26-03-2016.
 */
public class Constants {

    public final static String FRAGMENT_BLOOD = "blood";
    public final static String BLOOD = "blood_ref";
    public final static String FRAGMENT_SELECT_LANGUAGE = "language";
    public final static String FRAGMENT_SIGNUP = "signup";
    public final static String FRAGMENT_OTP = "otp";
    public final static String FRAGMENT_CREATE_EVENT = "createevent";
    public final static String FRAGMENT_PREVIEW_EVENT = "previewevent";
    public final static String FRAGMENT_VIEW_EVENT = "viewevent";
    public final static String FRAGMENT_SUBMIT_EVENT = "submitevent";

    public final static String FRAGMENT_CREATE_REQUEST = "createrequest";
    public final static String FRAGMENT_CLOSE_REQUEST = "requestclose";
    public final static String FRAGMENT_SUBMIT_REQUEST = "requestsubmit";
    public final static String FRAGMENT_FORWARD_REQUEST = "requestforward";
    public final static String FRAGMENT_MAIN_REQUEST = "requestmain";
    public final static String FRAGMENT_TRACK_REQUEST = "requesttrack";

    public final static String FRAGMENT_FORWARD_EVENT = "eventforward";

    public final static String FRAGEMNT_REQUEST_DETAILS = "requestdetails";

    public final static String FRAGMENT_GROUP_DETAILS = "groupdetail";
    public final static String FRAGMENT_GROUP_CREATE = "groupcreate";
    public final static String FRAGMENT_GROUP_MAIN = "groupmain";

    public final static String FRAGMENT_EMERGENCY_SUBMIT = "emergencysubmit";

    public final static String HOME = "home";

    public final static String GOOGLE_PROJ_ID = "1051066370793";
    public final static String USERID = "UserId";
    public final static String SESSION = "Session";
    public final static String USERNAME = "username";
    public final static String NAME = "name";
    public final static String SEEKERS = "Seekers";
    public final static String DONORS = "donors";
    public final static String MYREQUESTS = "request";
    public final static String MYDONATIONS = "contributions";
    public final static String USERIMAGE = "usrimage";
    public final static String POINTS = "points";
    public final static String USER_LAT = "lat";
    public final static String USER_LNG = "lng";
    public final static String USER_ADDRESS = "address";
    public final static String ISACTIVATED = "isactivated";

    public final static String BASEURL = "http://liverequest.in/angels/";
    public final static String WEBURL = "http://liverequest.in/";
    public final static String PRIVACY_LINK="terms-of-use-privacy-policy/";
    public final static String LOCALE = "prefLanguage";
    public final static String DUMMY_NO = "1111111111";

    public final static String INPUT_DATE_TIME_FORMAT="yyyy-MM-dd hh:mm:ss";
    public final static String INPUT_DATE_FORMAT="yyyy-MM-dd";


}
