package yeppypot.mychildclasslib.core.di.components;

import android.content.SharedPreferences;

import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;
import yeppypot.mychildclasslib.core.di.modules.ApplicationModule;
import yeppypot.mychildclasslib.core.di.modules.NetworkModule;

/**
 * Created by Lokesh on 03-03-2016.
 * Network component to return the network related apis
 */

@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class})
public interface NetworkComponent {
    Retrofit retrofit();

    SharedPreferences sharedPreferences();

    Picasso picasso();
}
