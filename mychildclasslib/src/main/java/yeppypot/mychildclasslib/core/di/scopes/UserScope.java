package yeppypot.mychildclasslib.core.di.scopes;

import javax.inject.Scope;

/**
 * Created by Lokesh on 04-03-2016.
 */
@Scope
public @interface UserScope {
}