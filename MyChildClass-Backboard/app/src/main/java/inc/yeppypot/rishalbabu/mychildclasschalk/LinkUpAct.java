package inc.yeppypot.rishalbabu.mychildclasschalk;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class LinkUpAct extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_link);
    }
    public void linkup(View view) {
        Intent intent = new Intent(this, SchoolActivity.class);
        startActivity(intent);
    }
}
