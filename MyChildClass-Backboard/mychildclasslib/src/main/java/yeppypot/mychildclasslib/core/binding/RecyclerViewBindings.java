package yeppypot.mychildclasslib.core.binding;

import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.Collection;

import ioceane.angelforyou.AngelsApplication;
import ioceane.angelforyou.R;
import ioceane.angelforyou.core.Adapters.adapter.BindingRecyclerViewAdapter;
import ioceane.angelforyou.core.Adapters.adapter.ClickHandler;
import ioceane.angelforyou.core.Adapters.adapter.LongClickHandler;
import ioceane.angelforyou.core.Adapters.adapter.ScrollHandler;
import ioceane.angelforyou.core.Adapters.adapter.binder.ItemBinder;
import ioceane.angelforyou.utils.Constants;


public class RecyclerViewBindings
{
    private static final int KEY_ITEMS = -123;
    private static final int KEY_CLICK_HANDLER = -124;
    private static final int KEY_LONG_CLICK_HANDLER = -125;
    private static final int KEY_SCROLL_HANDLER=-126;

    @SuppressWarnings("unchecked")
    @BindingAdapter("items")
    public static <T> void setItems(RecyclerView recyclerView, Collection<T> items)
    {
        BindingRecyclerViewAdapter<T> adapter = (BindingRecyclerViewAdapter<T>) recyclerView.getAdapter();
        if (adapter != null)
        {
            adapter.setItems(items);
        }
        else
        {
            recyclerView.setTag(KEY_ITEMS, items);
        }
    }

    @SuppressWarnings("unchecked")
    @BindingAdapter("scrollHandler")
    public static <T> void setHandler(RecyclerView recyclerView, ScrollHandler<T> handler)
    {
        BindingRecyclerViewAdapter<T> adapter = (BindingRecyclerViewAdapter<T>) recyclerView.getAdapter();
        if (adapter != null)
        {
            adapter.setScrollHandler(handler);
        }
        else
        {
            recyclerView.setTag(KEY_SCROLL_HANDLER, handler);
        }
    }

    @SuppressWarnings("unchecked")
    @BindingAdapter("clickHandler")
    public static <T> void setHandler(RecyclerView recyclerView, ClickHandler<T> handler)
    {
        BindingRecyclerViewAdapter<T> adapter = (BindingRecyclerViewAdapter<T>) recyclerView.getAdapter();
        if (adapter != null)
        {
            adapter.setClickHandler(handler);
        }
        else
        {
            recyclerView.setTag(KEY_CLICK_HANDLER, handler);
        }
    }

    @SuppressWarnings("unchecked")
    @BindingAdapter("longClickHandler")
    public static <T> void setHandler(RecyclerView recyclerView, LongClickHandler<T> handler)
    {
        BindingRecyclerViewAdapter<T> adapter = (BindingRecyclerViewAdapter<T>) recyclerView.getAdapter();
        if (adapter != null)
        {
            adapter.setLongClickHandler(handler);
        }
        else
        {
            recyclerView.setTag(KEY_LONG_CLICK_HANDLER, handler);
        }
    }

    @SuppressWarnings("unchecked")
    @BindingAdapter("itemViewBinder")
    public static <T> void setItemViewBinder(RecyclerView recyclerView, ItemBinder<T> itemViewMapper)
    {
        Collection<T> items = (Collection<T>) recyclerView.getTag(KEY_ITEMS);
        ClickHandler<T> clickHandler = (ClickHandler<T>) recyclerView.getTag(KEY_CLICK_HANDLER);
        BindingRecyclerViewAdapter<T> adapter = new BindingRecyclerViewAdapter<>(itemViewMapper, items);

        if(clickHandler != null)
        {
            adapter.setClickHandler(clickHandler);
        }
        recyclerView.setAdapter(adapter);
    }

    @BindingAdapter("bind:imageProfileUrl")
    public static void loadProfileImage(ImageView imageView, String url) {
        if(url != null && !url.startsWith("http://")&& !url.startsWith("file://")&& !url.startsWith("content://"))
        {
            url = Constants.BASEURL+url;
        }
        Picasso picasso = AngelsApplication.getInstance(imageView.getContext()).getNetworkComponent().picasso();
        picasso.load(url)
                .placeholder(R.drawable.avatar)
                .fit()
                .centerCrop()
                .error(R.drawable.avatar)
                .into(imageView);
    }

    @BindingAdapter("bind:imageUrl")
    public static void loadImage(ImageView imageView, String url) {
        if(url != null && !url.startsWith("http://")&& !url.startsWith("file://")&& !url.startsWith("content://"))
        {
            url = Constants.BASEURL+url;
        }
        Picasso picasso = AngelsApplication.getInstance(imageView.getContext()).getNetworkComponent().picasso();
        picasso.load(url)
                .placeholder(R.mipmap.default_bg)
                .fit()
                .centerCrop()
                .error(R.mipmap.default_bg).into(imageView);
    }

    @BindingAdapter("bind:imageGroupUrl")
    public static void loadGroupImage(ImageView imageView, String url) {
        if(url != null && !url.startsWith("http://")&& !url.startsWith("file://")&& !url.startsWith("content://"))
        {
            url = Constants.BASEURL+url;
        }
        Picasso picasso = AngelsApplication.getInstance(imageView.getContext()).getNetworkComponent().picasso();
        picasso.load(url)
                .placeholder(R.drawable.group_icon)
                .fit()
                .centerCrop()
                .error(R.drawable.group_icon).into(imageView);
    }
}