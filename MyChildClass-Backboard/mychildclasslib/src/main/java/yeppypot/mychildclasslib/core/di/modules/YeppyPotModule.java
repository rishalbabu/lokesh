package yeppypot.mychildclasslib.core.di.modules;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import yeppypot.mychildclasslib.core.di.scopes.UserScope;
import yeppypot.mychildclasslib.core.network.YeppyPotService;

/**
 * Created by Lokesh on 04-03-2016.
 */
@Module
public class YeppyPotModule {

    @Provides
    @UserScope
    public YeppyPotService providesYeppyPotService(Retrofit retrofit) {
        return retrofit.create(YeppyPotService.class);
    }
}
