package yeppypot.mychildclasslib.core.di.components;

import dagger.Component;
import yeppypot.mychildclasslib.core.di.modules.YeppyPotModule;
import yeppypot.mychildclasslib.core.di.scopes.UserScope;

/**
 * Created by Lokesh on 04-03-2016.
 * ShutterComponent is main component and exposes the inject methods and it has a dependency of network component
 */
@UserScope // using the previously defined scope, note that @Singleton will not work
@Component(dependencies = NetworkComponent.class, modules = YeppyPotModule.class)
public interface YeppyPotComponent {
}