package yeppypot.mychildclasslib.core.network;

import android.content.Context;
import android.text.TextUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Lokesh on 05-04-2016.
 */
public class ServiceFactory {

    private static ServiceFactory instance;

    private ServiceFactory()
    {
    }

    public static ServiceFactory getInstance() {
        if (instance == null) {
            instance = new ServiceFactory();
        }
        return instance;
    }


}
