package yeppypot.mychildclasslib.core.adapters.recycleradapter;

import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.databinding.ViewDataBinding;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.lang.ref.WeakReference;
import java.util.Collection;

import ioceane.angelforyou.R;
import ioceane.angelforyou.core.Adapters.adapter.binder.ItemBinder;
import ioceane.angelforyou.core.materialdesign.views.CheckBox;


public class BindingRecyclerViewAdapter<T> extends RecyclerView.Adapter<BindingRecyclerViewAdapter.ViewHolder> implements
        View.OnClickListener, View.OnLongClickListener {
    private static final int ITEM_MODEL = -124;
    private final WeakReferenceOnListChangedCallback onListChangedCallback;
    private final ItemBinder<T> itemBinder;
    private ObservableList<T> items;
    private LayoutInflater inflater;
    private ClickHandler<T> clickHandler;
    private LongClickHandler<T> longClickHandler;
    private ScrollHandler<T> scrollHandler;

    public BindingRecyclerViewAdapter(ItemBinder<T> itemBinder, @Nullable Collection<T> items) {
        this.itemBinder = itemBinder;
        this.onListChangedCallback = new WeakReferenceOnListChangedCallback<>(this);
        setItems(items);
    }

    public ObservableList<T> getItems() {
        return items;
    }

    public void setItems(@Nullable Collection<T> items) {
        if (this.items == items) {
            return;
        }

        if (this.items != null) {
            this.items.removeOnListChangedCallback(onListChangedCallback);
            notifyItemRangeRemoved(0, this.items.size());
        }

        if (items instanceof ObservableList) {
            this.items = (ObservableList<T>) items;
            notifyItemRangeInserted(0, this.items.size());
            this.items.addOnListChangedCallback(onListChangedCallback);
        } else if (items != null) {
            this.items = new ObservableArrayList<>();
            this.items.addOnListChangedCallback(onListChangedCallback);
            this.items.addAll(items);
        } else {
            this.items = null;
        }
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        if (items != null) {
            items.removeOnListChangedCallback(onListChangedCallback);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        if (recyclerView != null) {
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    RecyclerView.LayoutManager manager = recyclerView.getLayoutManager();
                    if (manager instanceof LinearLayoutManager) {
                        if (((LinearLayoutManager) manager).findLastCompletelyVisibleItemPosition() == manager.getChildCount()) {
                            if (scrollHandler != null) {
                                scrollHandler.onScrollEnd((T) manager.getChildAt(manager.getChildCount() - 1));
                            }
                        }
                    }
                }
            });
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int layoutId) {
        if (inflater == null) {
            inflater = LayoutInflater.from(viewGroup.getContext());
        }

        ViewDataBinding binding = DataBindingUtil.inflate(inflater, layoutId, viewGroup, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        final T item = items.get(position);
        viewHolder.binding.setVariable(itemBinder.getBindingVariable(item), item);
        viewHolder.binding.getRoot().setTag(ITEM_MODEL, item);
        viewHolder.binding.getRoot().setOnClickListener(this);
        viewHolder.binding.getRoot().setOnLongClickListener(this);
        View view = viewHolder.binding.getRoot().findViewById(R.id.acceptBtn);
        if (view != null) {
            view.setTag(ITEM_MODEL, item);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    T item = (T) v.getTag(ITEM_MODEL);
                    clickHandler.onItemButtonClick(v, item);
                }
            });
        }
        View rejView = viewHolder.binding.getRoot().findViewById(R.id.rejectBtn);
        if (rejView != null) {
            rejView.setTag(ITEM_MODEL, item);
            rejView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    T item = (T) v.getTag(ITEM_MODEL);
                    clickHandler.onItemButtonClick(v, item);
                }
            });
        }

        View callBtn = viewHolder.binding.getRoot().findViewById(R.id.callBtn);
        if (callBtn != null) {
            callBtn.setTag(ITEM_MODEL, item);
            callBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    T item = (T) v.getTag(ITEM_MODEL);
                    clickHandler.onItemButtonClick(v, item);
                }
            });
        }

        CheckBox checkBox = (CheckBox) viewHolder.binding.getRoot().findViewById(R.id.selectGroup);
        if (checkBox != null) {
            checkBox.setTag(ITEM_MODEL, item);
            checkBox.setOncheckListener(new CheckBox.OnCheckListener() {
                @Override
                public void onCheck(CheckBox view, boolean check) {
                    T item = (T) view.getTag(ITEM_MODEL);
                    clickHandler.onItemButtonClick(view, item);
                }
            });
        }

        Toolbar toolbar = (Toolbar) viewHolder.binding.getRoot().findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.getMenu().clear();
            toolbar.inflateMenu(R.menu.menu_group_action);
            toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem i) {
                    clickHandler.onItemMenuClick(i,item);
                    return false;
                }
            });
        }

        ImageView delete=(ImageView)viewHolder.binding.getRoot().findViewById(R.id.deleteContribution);
        if(delete != null)
        {
            delete.setTag(ITEM_MODEL, item);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    T item = (T) view.getTag(ITEM_MODEL);
                    clickHandler.onItemButtonClick(view, item);
                }
            });
        }

        ImageButton button = (ImageButton)viewHolder.binding.getRoot().findViewById(R.id.requestInfo);
        if(button != null)
        {
            button.setTag(ITEM_MODEL, item);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    T item = (T) view.getTag(ITEM_MODEL);
                    clickHandler.onItemMenuClick(null, item);
                }
            });
        }

        viewHolder.binding.executePendingBindings();
    }

    @Override
    public int getItemViewType(int position) {
        return itemBinder.getLayoutRes(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    @Override
    public void onClick(View v) {
        if (clickHandler != null) {
            T item = (T) v.getTag(ITEM_MODEL);
            clickHandler.onClick(item);
            clickHandler.onItemButtonClick(v, item);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (longClickHandler != null) {
            T item = (T) v.getTag(ITEM_MODEL);
            longClickHandler.onLongClick(item);
            return true;
        }
        return false;
    }

    public void setClickHandler(ClickHandler<T> clickHandler) {
        this.clickHandler = clickHandler;
    }

    public void setLongClickHandler(LongClickHandler<T> clickHandler) {
        this.longClickHandler = clickHandler;
    }

    public void setScrollHandler(ScrollHandler<T> scrollHandler) {
        this.scrollHandler = scrollHandler;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        final ViewDataBinding binding;

        ViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private static class WeakReferenceOnListChangedCallback<T> extends ObservableList.OnListChangedCallback {

        private final WeakReference<BindingRecyclerViewAdapter<T>> adapterReference;

        public WeakReferenceOnListChangedCallback(BindingRecyclerViewAdapter<T> bindingRecyclerViewAdapter) {
            this.adapterReference = new WeakReference<>(bindingRecyclerViewAdapter);
        }

        @Override
        public void onChanged(ObservableList sender) {
            RecyclerView.Adapter adapter = adapterReference.get();
            if (adapter != null) {
                adapter.notifyDataSetChanged();
            }
        }

        @Override
        public void onItemRangeChanged(ObservableList sender, int positionStart, int itemCount) {
            RecyclerView.Adapter adapter = adapterReference.get();
            if (adapter != null) {
                adapter.notifyItemRangeChanged(positionStart, itemCount);
            }
        }

        @Override
        public void onItemRangeInserted(ObservableList sender, int positionStart, int itemCount) {
            RecyclerView.Adapter adapter = adapterReference.get();
            if (adapter != null) {
                adapter.notifyItemRangeInserted(positionStart, itemCount);
            }
        }

        @Override
        public void onItemRangeMoved(ObservableList sender, int fromPosition, int toPosition, int itemCount) {
            RecyclerView.Adapter adapter = adapterReference.get();
            if (adapter != null) {
                adapter.notifyItemMoved(fromPosition, toPosition);
            }
        }

        @Override
        public void onItemRangeRemoved(ObservableList sender, int positionStart, int itemCount) {
            RecyclerView.Adapter adapter = adapterReference.get();
            if (adapter != null) {
                adapter.notifyItemRangeRemoved(positionStart, itemCount);
            }
        }
    }
}