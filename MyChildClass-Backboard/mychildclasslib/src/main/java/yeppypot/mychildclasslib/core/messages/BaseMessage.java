package yeppypot.mychildclasslib.core.messages;

/**
 * Created by Lokesh on 26-03-2016..
 */
public abstract class BaseMessage {

    public abstract String getFragmentName();
}
