package yeppypot.mychildclasslib.core.adapters.recycleradapter.binder;

import ioceane.angelforyou.network.ResponseModels.IHomeItem;
import ioceane.angelforyou.network.ResponseModels.Request;

/**
 * Created by Lokesh on 10-05-2016.
 */
public class RequestBinder extends ConditionalDataBinder<IHomeItem> {
    public RequestBinder(int bindingVariable, int layoutId) {
        super(bindingVariable, layoutId);
    }

    @Override
    public boolean canHandle(IHomeItem model) {
        return model instanceof Request;
    }
}
