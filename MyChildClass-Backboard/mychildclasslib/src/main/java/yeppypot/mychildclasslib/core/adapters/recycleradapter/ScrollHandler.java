package yeppypot.mychildclasslib.core.adapters.recycleradapter;

public interface ScrollHandler<T>
{
    void onScrollEnd(T lastViewModel);
}
