package inc.yeppypot.rishal.mychildclass;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MapsActivity.class);
                startActivity(intent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        switch (item.getItemId()) {
            case R.id.action_settings:
                // User chose the "Settings" item, show the app settings UI...
                Intent intent = new Intent(MainActivity.this,SettingsActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_status:
                // User chose the "Favorite" action, mark the current item
                // as a favorite...
                intent = new Intent(MainActivity.this,StatusActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_instant:
                // User chose the "Favorite" action, mark the current item
                // as a favorite...
                intent = new Intent(MainActivity.this,InstantActivity.class);
                startActivity(intent);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_childprofile) {
            Intent intent = new Intent(MainActivity.this,ChildProfileActivity.class);
            startActivity(intent);
            // Handle the camera action
        }
        if (id == R.id.nav_homework) {
            Intent intent = new Intent(MainActivity.this, HomeWorkActivity.class);
            startActivity(intent);
            // Handle the camera action
        } else if (id == R.id.nav_creativeassign) {
            Intent intent = new Intent(MainActivity.this, CreativeActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_absentnotice) {
            Intent intent = new Intent(MainActivity.this, AbsentNotice.class);
            startActivity(intent);

        } else if (id == R.id.nav_circulars) {
            Intent intent = new Intent(MainActivity.this, CircularActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_results) {
            Intent intent = new Intent(MainActivity.this, ChildResultActivity.class);
            startActivity(intent);

        }else if (id == R.id.nav_profiles) {
            Intent intent = new Intent(MainActivity.this, ProfilesActivity.class);
            startActivity(intent);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
