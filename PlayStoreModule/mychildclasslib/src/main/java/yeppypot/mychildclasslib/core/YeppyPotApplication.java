package yeppypot.mychildclasslib.core;

import android.app.Application;
import android.content.Context;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import yeppypot.mychildclasslib.core.di.components.NetworkComponent;
import yeppypot.mychildclasslib.core.di.components.YeppyPotComponent;
import yeppypot.mychildclasslib.core.di.modules.ApplicationModule;
import yeppypot.mychildclasslib.core.di.modules.NetworkModule;
import yeppypot.mychildclasslib.core.di.modules.YeppyPotModule;
import yeppypot.mychildclasslib.core.utils.Constants;

/**
 * Created by lokeshravichandru on 21/10/16.
 */

public class YeppyPotApplication extends Application {
    private NetworkComponent mNetComponent;
    private YeppyPotComponent yeppyPotComponent;

    public static YeppyPotApplication getInstance(Context context) {
        return (YeppyPotApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        CustomActivityOnCrash.install(this);
        CustomActivityOnCrash.setShowErrorDetails(true);
        CustomActivityOnCrash.setEnableAppRestart(true);
        mNetComponent = DaggerNetworkComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .networkModule(new NetworkModule(Constants.BASEURL))
                .build();

        yeppyPotComponent = DaggerYeppyPotComponent.builder()
                .networkComponent(mNetComponent)
                .angelsModule(new YeppyPotModule())
                .build();
    }
    /***
     * Get the shutter componenet instance
     *
     * @return ShutterComponent
     */
    public YeppyPotComponent getYeppyPotComponent() {
        return yeppyPotComponent;
    }

    /***
     * Get the Network component instance
     *
     * @return NetworkComponent
     */
    public NetworkComponent getNetworkComponent() {
        return mNetComponent;
    }
}
