package yeppypot.mychildclasslib.core.adapters.recycleradapter;

import android.view.MenuItem;
import android.view.View;

public interface ClickHandler<T>
{
    void onClick(T viewModel);
    void onItemButtonClick(View view, T viewModel);
    void onItemMenuClick(MenuItem item, T viewModel);
}