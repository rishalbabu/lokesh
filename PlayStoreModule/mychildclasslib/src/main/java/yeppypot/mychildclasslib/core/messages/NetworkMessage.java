package yeppypot.mychildclasslib.core.messages;

/**
 * Created by Lokesh on 28-04-2016.
 */
public class NetworkMessage {
    private boolean isNetworkOn;

    public NetworkMessage(boolean isNetworkOn) {
        this.isNetworkOn = isNetworkOn;
    }

    public boolean isNetworkOn() {
        return this.isNetworkOn;
    }
}
